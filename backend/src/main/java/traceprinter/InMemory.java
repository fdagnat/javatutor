package traceprinter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaFileObject;

import com.sun.jdi.Bootstrap;
import com.sun.jdi.VirtualMachine;
import com.sun.jdi.connect.Connector;
import com.sun.jdi.connect.IllegalConnectorArgumentsException;
import com.sun.jdi.connect.LaunchingConnector;
import com.sun.jdi.connect.VMStartException;

import traceprinter.ramtools.CompileToBytes;

public class InMemory {

	public List<String []>  classfiles;
	String usercode;
	JsonObject optionsObject;
	JsonArray argsArray;
	String givenStdin;
	String mainClass;
	VirtualMachine vm;
	static String stdin;
	Map<String, byte[]> bytecode;
	private String compileErrorMsg;

	public static String getFileContents(String filename) {
		StringBuilder result = new StringBuilder();
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			String line = null;
			while ((line = br.readLine()) != null) {
				result.append(line).append("\n");
			}
		} catch (Exception e) {
			throw new RuntimeException("getFileContents " + filename
					+ " failed: " + e.toString());
		}
		return result.toString();
	}

	public static String runMultifilesInstance(String jsonString) {

	    System.out.println("------ New Instance Started ------");

		JDI2JSON.userlog("Debugger VM maxMemory: "
				+ Runtime.getRuntime().maxMemory() / 1024 / 1024 + "M");

		traceprinter.shoelace.NoopMain.main(null);

		InMemory im = new InMemory(Json.createReader(new StringReader(jsonString)).readObject());

		// compile error, the VM is not lanched
		if (im.vm == null) {
			return im.compileErrorMsg;
		}

		JSONTracingThread tt = new JSONTracingThread(im);
		tt.start();
		im.resumeVM();

		// get the result from the tt
		while (tt.isConnected()) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	    System.out.println("--- Finished ---");

		return tt.getOutputString();
	}

	// convenience version of JDI2JSON method
	void compileError(String msg, long row, long col) {
		try {
			JsonObject jo = JDI2JSON.compileErrorOutput(usercode, msg, row, col);
			PrintStream out = new PrintStream(System.out, true, "UTF-8");
			compileErrorMsg = jo.toString();
			out.print(compileErrorMsg);
		} catch (UnsupportedEncodingException e) { // fallback
			System.out.print(JDI2JSON.compileErrorOutput(usercode, msg, row,
					col));
		}
	}

	void fileCompileError(String msg, String filename, long row, long col) {
		try {
			JsonObject jo = JDI2JSON.fileCompileErrorOutput(classfiles, msg, filename, row, col);
			PrintStream out = new PrintStream(System.out, true, "UTF-8");
			compileErrorMsg = jo.toString();
			out.print(compileErrorMsg);
		} catch (UnsupportedEncodingException e) { // fallback
			System.out.print(JDI2JSON.fileCompileErrorOutput(classfiles, msg, filename, row, col));
		}
	}


	private JsonArray usercodes;


	// figure out the class name, then compile and run main([])
	public InMemory(JsonObject frontend_data) {

		usercodes = frontend_data.getJsonArray("usercode");

		// FIXME: can only compile class
		Pattern pattern = Pattern.compile("public\\s+class\\s+([a-zA-Z0-9_]+)\\b");
		Pattern mainClassPattern = Pattern.compile(
				"public\\s+static\\s+void\\s+main\\s*\\("
				+ "\\s*String\\s*\\[\\]\\s+[a-zA-Z0-9_]+\\)");

		classfiles = new ArrayList<String[]>();

		StringBuilder sb = new StringBuilder();

		mainClass = null;

		for (JsonValue v: usercodes) {
			Matcher m = pattern.matcher(v.toString());
			if (!m.find()) {
				System.out.println("class not find!");
			}
			String klass = m.group(1);

			Matcher m2 = mainClassPattern.matcher(v.toString());
			if (m2.find()) {
				System.out.println("mainclass:"  + klass);
				if (mainClass == null) {
					mainClass = klass;
				} else {
					System.out.println("Multiple main class found:" + klass);
				}
			}

			for (String S : JDI2JSON.PU_stdlib) {
				if (klass.equals(S)) {
					compileError("You cannot use a class named " + S + " since it conflicts with a 'stdlib' class name",
							1, 1);
					return;
				}
			}

			classfiles.add(new String[] {klass, v.toString()});
			sb.append(v.toString());
		}

		// legacy attribute
		usercode = sb.toString();

		this.optionsObject = frontend_data.getJsonObject("options");
		this.argsArray = frontend_data.getJsonArray("args");
		this.givenStdin = frontend_data.getJsonString("stdin").getString();
		stdin = this.givenStdin;

		if (frontend_data.containsKey("visualizer_args")
				&& (!frontend_data.isNull("visualizer_args"))) {
			JsonObject visualizer_args = frontend_data
					.getJsonObject("visualizer_args");

			if (visualizer_args.getJsonNumber("MAX_STEPS") != null)
				JSONTracingThread.MAX_STEPS = visualizer_args
						.getJsonNumber("MAX_STEPS").intValue();

			if (visualizer_args.getJsonNumber("MAX_STACK_SIZE") != null)
				JSONTracingThread.MAX_STACK_SIZE = visualizer_args
						.getJsonNumber("MAX_STACK_SIZE").intValue();

			if (visualizer_args.getJsonNumber("MAX_WALLTIME_SECONDS") != null)
				JSONTracingThread.MAX_WALLTIME_SECONDS = visualizer_args
						.getJsonNumber("MAX_WALLTIME_SECONDS").intValue();
		}

		// not 100% accurate, if people have multiple top-level classes + public

		CompileToBytes c2b = new CompileToBytes();

		c2b.compilerOutput = new StringWriter();
		c2b.options = Arrays.asList("-g", "-Xmaxerrs", "1");// ,"-classpath",System.getProperty("java.class.path"));

		DiagnosticCollector<JavaFileObject> errorCollector = new DiagnosticCollector<>();
		c2b.diagnosticListener = errorCollector;

		/*
		 * For some reason the JVM at Princeton doesn't actually figure out how
		 * to read these particular files off its classpath. So we'll just throw
		 * them all in there manually. TODO: Optimize and only use files
		 * actually referenced by student code.
		 */
		boolean isPrinceton = System.getProperty("java.class.path").contains(
				"cos126");

		if (isPrinceton) {
			String[][] fileinfo = new String[][] {
					{ "Stack",
							getFileContents("cp/visualizer-stdlib/Stack.java") },
					{ "Queue",
							getFileContents("cp/visualizer-stdlib/Queue.java") },
					{ "ST", getFileContents("cp/visualizer-stdlib/ST.java") },
					{ "StdIn",
							getFileContents("cp/visualizer-stdlib/StdIn.java") },
					{ "StdOut",
							getFileContents("cp/visualizer-stdlib/StdOut.java") },
					{
							"Stopwatch",
							getFileContents("cp/visualizer-stdlib/Stopwatch.java") },
					{ mainClass, usercode } };
			bytecode = c2b.compileFiles(fileinfo);
		} else {
			// do the normal thing
			double start = System.currentTimeMillis();
			bytecode = c2b.compileFiles(classfiles.toArray(new String[classfiles.size()][]));
			double end = System.currentTimeMillis();
			System.out.println("compile time:" + (end - start));
		}

		if (bytecode == null) {
			System.out.println("Compile Error!!");

			for (Diagnostic<? extends JavaFileObject> err : errorCollector.getDiagnostics()) {
				if (err.getKind() == Diagnostic.Kind.ERROR) {
					fileCompileError("Error: " + err.getMessage(null),
                                        err.getSource().getName(),
                                        Math.max(0, err.getLineNumber()),
                                        Math.max(0, err.getColumnNumber()));
					return;
				}
			}

			compileError("Compiler did not work, but reported no ERROR?!?!", 0, 0);
			return;
		}

		if (mainClass == null) {
			compileError("No main method found!", 0, 0);
			return;
		}

		vm = launchVM("traceprinter.shoelace.NoopMain");
		vm.setDebugTraceMode(0);
	}

	public void resumeVM() {
		vm.resume();
	}

	VirtualMachine launchVM(String className) {
		LaunchingConnector connector = theCommandLineLaunchConnector();
		try {

			java.util.Map<String, Connector.Argument> args = connector
					.defaultArguments();

			/*
			 * what are the other options? on my system,
			 *
			 * for (java.util.Map.Entry<String, Connector.Argument> arg:
			 * args.entrySet()) { System.out.print(arg.getKey()+" ");
			 * System.out.print("["+arg.getValue().value()+"]: ");
			 * System.out.println(arg.getValue().description()); }
			 *
			 * prints out:
			 *
			 * home [/java/jre]: Home directory of the SDK or runtime
			 * environment used to launch the application options []: Launched
			 * VM options main []: Main class and arguments, or if -jar is an
			 * option, the main jar file and arguments suspend [true]: All
			 * threads will be suspended before execution of main quote ["]:
			 * Character used to combine space-delimited text into a single
			 * command line argument vmexec [java]: Name of the Java VM launcher
			 *
			 * For more info, see
			 * http://docs.oracle.com/javase/7/docs/jdk/api/jpda
			 * /jdi/com/sun/jdi/connect/Connector.Argument.html
			 */

			(args.get("main")).setValue(className);

			String options = "";

			// inherit the classpath. if it were not for this, the CLASSPATH
			// environment
			// variable would be inherited, but the -cp command-line option
			// would not.
			// note that -cp overrides CLASSPATH.

			options += "-cp " + System.getProperty("java.class.path") + " ";

			// set a memory limit

			options += "-Xmx512M" + " ";

			options += "-Dfile.encoding=UTF-8" + " ";

			options += "-Djava.awt.headless=true" + " ";

			(args.get("options")).setValue(options);

			// System.out.println("About to call LaunchingConnector.launch...");
			VirtualMachine result = connector.launch(args);
			// System.out.println("...done");
			return result;
		} catch (VMStartException exc) {
			System.out.println("Hoeyx!");
			System.out.println("Failed in launchTarget: " + exc.getMessage());
			exc.printStackTrace();
			byte[] b = new byte[100000];
			System.out.println(exc.process().exitValue());
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						exc.process().getErrorStream()));
				String inputLine;
				while ((inputLine = in.readLine()) != null)
					System.out.println(inputLine);
				in = new BufferedReader(new InputStreamReader(exc.process()
						.getInputStream()));
				while ((inputLine = in.readLine()) != null)
					System.out.println(inputLine);

			} catch (java.io.IOException excx) {
				System.out.println("Crud");
			}
		} catch (java.io.IOException exc) {
			System.out.println("Failed in launchTarget: " + exc.getMessage());
			exc.printStackTrace();
		} catch (IllegalConnectorArgumentsException exc) {
			System.out.println("Hoeyy!");
			for (String S : exc.argumentNames()) {
				System.out.println(S);
			}
			System.out.println(exc);
		}
		return null; // when caught
	}

	LaunchingConnector theCommandLineLaunchConnector() {
		for (Connector connector : Bootstrap.virtualMachineManager()
				.allConnectors())
			if (connector.name().equals("com.sun.jdi.CommandLineLaunch"))
				return (LaunchingConnector) connector;
		throw new Error("No launching connector");
	}
}
