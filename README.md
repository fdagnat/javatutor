# JavaTutor

JavaTutor is a tool for visualizing the execution of Java programs on the Web.

It aims at helping people to learn object-oriented programming (OOP) by showing what computer does
when executing each line of a Java program.

**Authors**

See [contributers](./contributors.txt) file.

**Licence**

See [licence](./LICENCE) file


# Get started

First of all, make sure that you have `java 8` installed on your Linux system,
because the server backend runs on `java 8`.


**Vitual Machine**

If you don't have a Linux system,
click [here](https://partage.mines-telecom.fr/public.php?service=files&t=6a5a5e89d086f1aa6d9b14f88502999b)
to download a virtual machine with JavaTutor and Java 8 already installed.

The username and the password of this VM are both `jtutor`.

This VM has also all the dependencies to build the project, in case that you want to hack the code :)


**Get the code**

You can download a zip file directely from the repository.
Checkout the `Tags` on [this link](https://bitbucket.org/fdagnat/javatutor/downloads).

You can also clone the project:

    $> git clone https://yuancheng@bitbucket.org/fdagnat/javatutor.git

To see all the tags:

    $> git tag

For example, let's checkout `v0.1.0`:

    $> git fetch && git checkout v0.1.0


**Run the server**

Go into the release directory and run the server:

    $> cd javatutor/RELEASE/
    $> java -jar javatutor-x.x.x.jar

By default, the server will run on the port `8080`.
If you wish to run on another port, you can specify by using `-Dserver.port` argumement.

For example, if you want to run the server on `9000`:

    $> java -Dserver.port=9000 -jar javatutor-x.x.x.jar


**Use it!**

Open the page `javatutor/RELEASE/dist/index.html` on your browser.

If you have run your server on a port other than `8080`, you should specify the address on the
`Configuration` page.

For the example above, we should configure to `http://localhost:9000`.

It's very easy to use: you write your code, then you send the code to the server.

![write_code](./images/write_code.png)


The server will response a trace to you so that you can start your visualization.

![visualization](./images/visualization.png)


# Developer

If you want to contribute to the project or hack the project or just want to know how it works,
please find out the [developer's doc](doc/).
