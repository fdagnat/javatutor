/**
 * Created by Yuancheng Peng, March 2016
 */


'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:VisualizeCtrl
 * @description
 * # VisualizeCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('VisualizeCtrl',
    ['$scope', '$timeout', 'jtPlumb', 'jtHelper', 'localStorageService',
    function ($scope, $timeout, jtPlumb, jtHelper, localStorageService) {

    var DATA_STRING_INLINED = 'data_string_inlined';
    var stackRefs = [], heapRefs = [];

    // TODO if no data, then go back to edit code page.
    var data = localStorageService.get('data');
    data = jtHelper.inlineString(data); // default config

    localStorageService.set(DATA_STRING_INLINED, data);

    var gotoStep = function (step, backward) {
      if (!backward || backward === 'undefined') { // go forward
        $scope.preTrace = $scope.curTrace;
      } else { // backward
        $scope.preTrace = undefined;
      }

      $scope.curStep = step;
      $scope.curTrace = data.trace[step];
    };

    $scope.showArrows = true;

    $scope.toggleShowArrows = function (sa) {
      console.log('show arrows:' + sa);
      $scope.showArrows = sa;
      if (sa) {
        jtPlumb.reset();
        var trace = $scope.curTrace;
        var stack_to_render = trace["stack_to_render"];
        $scope.activateStack(stack_to_render[0]['frame_id']);
      } else {
        jtPlumb.reset();
      }

    };

    $scope.toggleShowString = function (showStringObj) {
      if (showStringObj) {
        data = localStorageService.get('data')
      } else {
        data = localStorageService.get(DATA_STRING_INLINED);
      }
      gotoStep($scope.curStep);
    };

    $scope.nextStep = function () {
      if ($scope.curStep === data.trace.length - 1) {
        console.log("end of trace");
        return;
      }
      var next= $scope.curStep + 1;
      // skip the call trace
      if (data.trace[next].event === 'call') {
        next += 1;
      }
      gotoStep(next);
    };

    $scope.previousStep = function () {
      if ($scope.curStep == 1) {
        console.log("beginning of trace");
        return;
      }
      var prev = $scope.curStep - 1;
      // skip the call trace
      if (data.trace[prev].event === 'call') {
        prev -= 1;
      }
      prev = prev < 1 ? 1 : prev;
      gotoStep(prev, true);
    };

    $scope.stepOver = function () {
      if ($scope.curStep === data.trace.length - 1) {
        console.log("end of trace");
        return;
      }
      var nextStep = $scope.curStep + 1;
      if (data.trace[nextStep]["event"] !== 'call') {
        $scope.nextStep();
        return;
      } else {
        var count = 1;
        // find next 'return' trace
        while (count !== 0) {
          nextStep += 1;
          if (data.trace[nextStep]['event'] === 'call') {
            count += 1;
          } else if (data.trace[nextStep]['event'] === 'return') {
            count -= 1;
          }
        }
        gotoStep(nextStep + 1);
      }
    };

    $scope.backOver = function () {
      if ($scope.curStep <= 1) {
        console.log("beginning of trace");
        return;
      }
      var nextStep = $scope.curStep - 1;
      if (data.trace[nextStep]["event"] !== 'return') {
        $scope.previousStep();
        return;
      } else {
        var count = 1;
        // find previous 'call' trace
        while (count !== 0) {
          nextStep -= 1;
          if (data.trace[nextStep]['event'] === 'return') {
            count += 1;
          } else if (data.trace[nextStep]['event'] === 'call') {
            count -= 1;
          }
        }
        gotoStep(nextStep - 1, true);
      }
    };

    $scope.reset = function () {
      gotoStep(1, true);
    };

    $scope.selectTab = function (filename) {
      var tabs = $scope.tabs;
      for (var i = 0; i < tabs.length; i++) {
        if (tabs[i].filename === filename) {
          tabs[i].active = true;
          break;
        }
      }
    };

    // init scope
    gotoStep(1);
    $scope.tabs = [];
    // embed the code into the tabs
    for (var fname in data.code)  {
      $scope.tabs.push({filename: fname, code: data.code[fname].split('\n')});
    }

    var createConnections = function (refs, isActive) {
      var i;
      if (isActive) {
        for (i = 0; i < refs.length; i++)
          jtPlumb.createActiveConn(refs[i], "TARGET" + refs[i].split('-')[3]);

      } else {
        for (i = 0; i < refs.length; i++)
          jtPlumb.createInactiveConn(refs[i], "TARGET" + refs[i].split('-')[3]);

      }
    };

    $scope.activateStack = function (frameId) {
      if ($scope.showArrows === false) {
        return;
      }

      var trace = $scope.curTrace;
      var stack_to_render = trace.stack_to_render;
      var frame, i;

      if (frameId !== 'STATIC') {
        // find frame reference by id
        for (i = 0; i < stack_to_render.length; i++) {
          frame = stack_to_render[i];
          if (frame['frame_id'] === frameId) {
            break;
          }
        }
        if (i === stack_to_render.length) {
          throw Error("frame not found");
        }
      }

      // get all the refs of this frame
      var activeStackRefs = [];
      var inactiveStackRefs = [];
      var s;
      for (i = 0; i < stackRefs.length; i++) {
        s = stackRefs[i].split('-');
        if (s[1] === '' + frameId) {
          activeStackRefs.push(stackRefs[i]);
        } else {
          inactiveStackRefs.push(stackRefs[i]);
        }
      }

      jtPlumb.reset();
      createConnections(activeStackRefs, true);
      createConnections(inactiveStackRefs, false);
      createConnections(heapRefs, false);
    };

    var _searchSourceToRef = function (sourceRefs, ref, activeLinks, inactiveLinks) {
      var i, s;
      for (i = 0; i < sourceRefs.length; i++) {
        s = sourceRefs[i].split('-');
        if (s[3] == ref) {
          activeLinks.push(sourceRefs[i]);
        } else {
          inactiveLinks.push(sourceRefs[i]);
        }
      }
    };

    var _wrapObjActivation = function (ref, value, func) {
      if ($scope.showArrows === false) {
        return;
      }
      var activeLinks = [], inactiveLinks = [];
      _searchSourceToRef(heapRefs, ref, activeLinks, inactiveLinks);
      _searchSourceToRef(stackRefs, ref, activeLinks, inactiveLinks);

      // do additional stuffs with the links
      if (typeof func === "function") {
        func(activeLinks, inactiveLinks);
      }

      jtPlumb.reset();
      createConnections(activeLinks, true);
      createConnections(inactiveLinks, false);
    };

    $scope.activateInstance = function (ref, value) {
      _wrapObjActivation(ref, value,
            function (activeLinks, inactiveLinks) {
        var i, l, index;
        for (i = 2; i < value.length; i++) {
          // value[i] -> [field, ['REF', ref_id]]
          if (value[i][1] !== null
            && value[i][1].constructor === Array
            && value[i][1][0] === "REF") {
            //<!--   SOURCE - INSTANCE_REF - VAR_NAME - REF_ID   -->
            l = ["SOURCE", ref, value[i][0], value[i][1][1]].join('-');
            activeLinks.push(l);
            // remove the link
            index = inactiveLinks.indexOf(l);
            if (index > -1)
              inactiveLinks.splice(index, 1);
          }
        }
      });
    };

    $scope.activateArray = function (ref, value) {
      _wrapObjActivation(ref, value,
            function (activeLinks, inactiveLinks) {
        var i, l, index;
        for (i = 1; i < value.length; i++) {
          // value[i] -> ['REF', ref_id]
          if (value[i] !== null && value[i].constructor === Array && value[i][0] === 'REF') {
            //<!--   SOURCE - ARRAY_REF - ARRAY_INDEX - REF_ID   -->
            l = ["SOURCE", ref, i, value[i][1]].join('-');
            activeLinks.push(l);
            // remove the link
            index = inactiveLinks.indexOf(l);
            if (index > -1)
              inactiveLinks.splice(index, 1);
          }
        }
      });
    };

    $scope.activateString = function (ref, value) {
      _wrapObjActivation(ref, value);
    };

    $scope.$watch('curTrace', function () {
      $timeout(function () {
        stackRefs = [];
        heapRefs = [];

        // connections 1: stack ref to
        var curTrace = $scope.curTrace;
        var i, stack_to_render = curTrace.stack_to_render;
        var call, encLocal;
        var key, value;
        // fetch all the "REF"
        for (i = 0; i < stack_to_render.length; i++) {
          call = stack_to_render[i];
          for (var localRefKey in call.encoded_locals) {
            encLocal = call.encoded_locals[localRefKey];
            if (encLocal && encLocal[0] === "REF") {

              //<!--   SOURCE - FRAME_ID - VAR_NAME - REF_ID   -->
              stackRefs.push([
                "SOURCE", call.frame_id, localRefKey, call.encoded_locals[localRefKey][1]
              ].join('-'));
            }
          }
        }

        // connections 2: static fields
        var staticFields = curTrace.globals;
        for (key in staticFields) {
          value = staticFields[key];
          if (value && value[0] === "REF") {
            stackRefs.push(["SOURCE-STATIC", key, value[1]].join('-'));
          }
        }

        // connections 3: heap objects
        var heap = curTrace.heap;

        /*
        heap: {
            id: ['INSTANCE',
                 class_name,
                 [ field_1, ['REF', ref_id] ],
                 [ field_2, primitive_value ],
                ],

            id: ['LIST',
                 primitive_1,
                 primitive_2,
                 ],

             id: ['LIST',
                  ['REF', ref_id],
                  ['REF', ref_id],
                 ],

             id [ "HEAP_PRIMITIVE",
                 "String",
                 "Hello"
                ],
         }
         */
        for (key in heap) {
          value = heap[key];
          if (value[0] == 'INSTANCE') {     // Class Instance
            for (i = 2; i < value.length; i++) {
              // value[i] -> [field, ['REF', ref_id]]
              if (value[i][1] !== null
                && value[i][1].constructor === Array
                && value[i][1][0] === "REF") {
                //<!--   SOURCE - INSTANCE_REF - VAR_NAME - REF_ID   -->
                heapRefs.push([
                  "SOURCE", key, value[i][0], value[i][1][1]
                ].join('-'));
              }
            }

          } else if (value[0] === 'LIST') {   // Array Object
            for (i = 1; i < value.length; i++) {
              // value[i] -> ['REF', ref_id]
              if (value[i] !== null
                  && value[i].constructor === Array
                  && value[i][0] === 'REF') {
                //<!--   SOURCE - ARRAY_REF - ARRAY_INDEX - REF_ID   -->
                heapRefs.push([
                  "SOURCE", key, i, value[i][1]
                ].join('-'));
              }
            }
          }
        }

        $scope.activateStack(stack_to_render[0]['frame_id']);
      }, 0);

      // ['REF', ref_id]
      $scope.isReference = function (a) {
        return a !== null
          && a !== 'undefined'
          && a.constructor === Array
          && a[0] === 'REF';
      }
    });
  }])

  .directive('jtStackHeap', function () {
    return {
      templateUrl: "views/stack_heap.html"
    }
  })

  .directive('jtCodeArea', function () {
    return {
      templateUrl: "views/code_area.html"
    }
  })

  .directive('jtScroll', function () {
    return {
      link: function (scope, element, attrs) {
        if (scope.curTrace === "undefined") {
          return;
        }
        var className = scope.curTrace.source.slice(0, -5);
        var el = $("#code_area_" + className);

        // change tab
        scope.selectTab(scope.curTrace.source);

        // scroll the cursor to the middle of the code area,
        // half of the max size of code_area's height.
        el.animate({
          scrollTop: element.offset().top - el.offset().top + el.scrollTop() - 150
        }, 200);
      }
    };
  })

  .filter('checkNull', function () {
    return function (input) {
      if (angular.isUndefined(input) || input == null) {
        input = 'null';
      }
      return input;
    }
  })

  .filter('isEmpty', function() {
    return function(object) {
      return angular.equals({}, object);
    }
  })

  .filter('removeExtension', function () {
    return function (input) {
      //input = input.replace(/\.[^/.]+$/, "");
      return input.slice(0, -5);
    }
  })

  .filter('reverse', function () {
    return function(items) {
      return items.slice().reverse();
    };
  })

  .filter('beautifyRef', function () {
    return function(ref, showArrow) {
      return  showArrow === true ? '' : '(Ref: ' + ref[1] + ')';
    };
  })
;
