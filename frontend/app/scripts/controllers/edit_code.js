/**
 * Created by Yuancheng Peng, on 12/31/15.
 */

'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:NewsCtrl
 * @description
 * # NewsCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('EditCtrl',
    ['$scope', '$http', '$location', 'localStorageService',
    function ($scope, $http, $location, localStorageService) {

      var DEFAULT_SERVER_URL = 'http://localhost:8080';
      var SERVER_URL = localStorageService.get('server_url') || DEFAULT_SERVER_URL;
      var DEFAULT_CODE = "public class YourClassName {\n\n}";

      var __global_id = new Date().getTime();
      var _loaded_editor_count = 0; // nasty solution. for loading existing code into editor

      var code = localStorageService.get('code') || {};
      var aceSessions = [];

      var getUniqueId = function () {
        __global_id += 1;
        return __global_id;
      };

      $scope.codeSent = false;


      var init = function () {
        $scope.tabs = [];

        // load existing code into tabs
        if (Object.keys(code).length > 0) {
          for (var fname in code) {
            $scope.tabs.push({
              id: getUniqueId(),//
              init_code: code[fname],
              heading: fname
            });
          }
        } else {
          $scope.tabs.push({id: getUniqueId()});  // at least one editor

        }
        // activate the last tab
        $scope.currentTabId = $scope.tabs.length - 1; // currentTabId is mainly used for deleting editor
        $scope.tabs[$scope.currentTabId].active = true;
      };

      init();

      $scope.addEditor = function () {
        $scope.tabs.push({id: new getUniqueId()});
      };

      $scope.deleteEditor = function () {
        var i = 0;
        for (; i < $scope.tabs.length; i++)
          if ($scope.tabs[i].id === $scope.currentTabId) break;

        if (i > -1 && i < $scope.tabs.length) {
          $scope.tabs.splice(i, 1);
          aceSessions.splice(i, 1);
        }
      };

      // callback
      $scope.selectTab = function(tabId) {
        $scope.currentTabId = tabId;
      };

      $scope.aceLoaded = function (_editor) {
        var session = _editor.getSession();
        aceSessions.push(session);

        // set the existing code
        if (_loaded_editor_count < Object.keys(code).length) {
          session.setValue($scope.tabs[_loaded_editor_count].init_code || '');
          _loaded_editor_count++;
        } else { // it's a new Tab
          session.setValue(DEFAULT_CODE);
           $scope.tabs[$scope.tabs.length - 1].active = true; // activate this tab
        }
      };

      $scope.sendCode = function () {
        $scope.codeSent = true;

        console.debug("sending code..");

        var usercode = [];
        var c;
        for (var i = 0; i < aceSessions.length; i++) {
          c = aceSessions[i].getValue().trim();
          if (c != "") {
            usercode.push(c);
          }
          // TODO do some check before sending the code
        }

        var input = {
          usercode: usercode,
          options: {},
          args: [],
          stdin: ""
        };

        $http({
          url: SERVER_URL + "/code",
          method: "POST",
          data: input
        }).success(function (data, status, headers, config) {
          $scope.codeSent = false;
          console.log("post succeed!");

          var trace = data["trace"];
          var errMsg = "";
          var curTrace;
          for (var i = 0; i < trace.length; i++) {
            curTrace = trace[i];
            if (curTrace["exception_msg"]) {
              if (curTrace["source"] || curTrace["line"] || curTrace["offset"]) {
                errMsg += "source:" + curTrace["source"]
                  + ", line:" + curTrace["line"]
                  + ", offset:" + curTrace["offset"] + "\n";
              }
              errMsg += curTrace["exception_msg"] + "\n";
            }
          }

          if (errMsg !== '') {
            alert(errMsg);
            return;
          }

          localStorageService.set('data', data);
          localStorageService.set('code', data.code);

          $location.path('/visualize');

        }).error(function (data, status, headers, config) {
          $scope.codeSent = false;
          if (status === 500) {
            alert(data["error"] || data);
          } else if (status === -1) {
            alert("The server '" + SERVER_URL + "' is not reachable.");
          } else {
            alert("An unknown has occured :(");
          }
          console.log("post error!");
        });
      };
    }]
  )
;
