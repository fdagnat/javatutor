/**
 * Created by Yuancheng Peng, March 2016
 */


'use strict';

/**
 * @ngdoc overview
 * @name frontendApp
 * @description
 * # frontendApp
 *
 * Main module of the application.
 */
angular
  .module('frontendApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.ace',
    'ui.bootstrap',
    'LocalStorageModule'
  ])
  .config(["$routeProvider", function ($routeProvider) {
    $routeProvider
      .when('/edit_code', {
        templateUrl: 'views/edit_code.html',
        controller: 'EditCtrl',
        controllerAs: 'edit_code'
      })
      .when('/visualize', {
        templateUrl: 'views/visualize.html',
        controller: 'VisualizeCtrl',
        controllerAs: 'visualize'
      })
      .when('/config', {
        templateUrl: 'views/config.html',
        controller: 'ConfigCtrl',
        controllerAs: 'config'
      })
      .otherwise({
        redirectTo: '/edit_code'
      });
  }])
  .directive('ngConfirmClick', [ function(){
    return {
      priority: -1,
      restrict: 'A',
      link: function(scope, element, attrs){
        element.bind('click', function(e){
          var message = attrs.ngConfirmClick;
          if(message && !confirm(message)){
            e.stopImmediatePropagation();
            e.preventDefault();
          }
        });
      }
    }
  }
  ]);


// load jsPlumb before starting angular app
jsPlumb.ready(function(){
  angular.element(document).ready(function() {
    // bootstrap here, don't declare `ng-app` in index.html,
    // otherwise it will be loaded twice.
    angular.bootstrap(document, ['frontendApp']);

    $(window).resize(function() {
      // TODO: reset jsPlumb zoom
    });

  });
});
