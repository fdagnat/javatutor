# Planning

## Organization

As there are a lot of unknown technologies in this project,
it's very hard to imagine the quantity of work behind the scene.
It's also hard to guarantee whether we can finish all the features
previously defined before the deadline.

Therefore, we must react agilely and develop iteratively.

All the code and documentations will be managed on `bitbucket`.
In additional, we will create `trello` boards to manage the tasks.


## Versions planning

Let's break things roughly into multiple versions and extimate the time needed
for the development.

**v0.1**
(32h)

* Set up an overall client-server architecture with the frameworks. (8)
* Use the `jdi` package to generate the JSON block by block during the execution. (10)
* The server compiles and runs multiple classes. (6)
* The UI can edit multiple classes. (8)

This version responds to the following features:

* Much more steps of execution
* Less waiting time before the visualization
* Multiple files for multiple classes


**v0.2**
(30h)

In this version, we consider only one thread.

* Given JSON data, visualize the execution step by step.
    - step-in (2)
    - step-over (4)
    - go back to previous steps (2)

The UI contains at least the following elements:

- A code area, with a cursor highlighting the next line to execute.
    The code area contains multiple files, thus it's necessary to consider jumping between
    different files. (8)
- A stack of frames. (4)
- A graph of objects. (6)
- Links between references and objects. (4)



**v0.3**
(30h)

* Options for showing different levels of detail.

These options need to be discussed and designed attentively during the development.

**v0.4**
(15h)

* The server can track multi-thread programs.
* Show one stack of frames for each thread.


To conclude, we assume that we will need:

- 107 hours of development time for all the features.
- 92 hours of development time if we cut off multi-thread support.

We must be aware that the estimation time shown above is subjective and inaccurate.
In the meanwhile, the time needed for documentation, preparation of presentation and demonstration are not yet counted.

## Milestones

* 21/11/2015: Report (Project context, analysis, planning)
* 19/12/2015: Oral presentation
* 25/02/2016: Prototype demonstration
* 03/11/2016: Technical report and final demonstration

Gantt diagram (Overall organazation )

![gantt-19-11](images/planning/gantt-19-11.png)

