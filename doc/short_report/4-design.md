# Technologies

Initially, we wanted to reuse certain components of `PythonTutor/Java`.
However, after the previous analysis, we notice that it would be simpler to start from zero
(and hoping that we will do better!).

## Backend

**Java tracer**

`PythonTutor/Java` uses `java_jail/tracer` as their backend to trace the execution.

We need to hack into `java_jail/tracer`, for two purposes:

* Export the results block by block during the execution of a program.
* Provide more trace information in the JSON so that the Front-end can control
    the view without re-launching the program.

Note that breaking the JSON into blocks need to redesign the JSON's structure.
We might even totally re-write this the tracer by using `com.sun.jdi` package.


**Web framework**

Since the codes are run on a Java Virtual Machine (JVM), it would be better to
use a Java based Web framework instead of other languages.

Note that our application does not rely too much on frameworks, using simple Java
servlets may also be sufficient.  However, using frameworks might be faster for the development.

We have several excellent candidate choices: `Spring MVC`, `Grails`, `Play framework`.

## Front-end

`JavaTutor` would be a typical Single Page Application(SPA).

* We choose `AngularJS` framework for its clear MVC(Model-View-Controler) code structure and popularity.
* To provide a web-based code editor, we use [`ace`](https://ace.c9.io/), which is easy to embed and offers syntax highlighting.
* To  visualize the objects and links between them, we can use [`jsplumb`](https://github.com/sporritt/jsplumb/).
* `jQuery` is certainly indispensable for managing AJAX calls and DOM objects.
* `Bootstrap` is also needed to ease the development of the Web interface.


[next chapter](./5-planning.md)

