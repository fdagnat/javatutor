# Analysis of PythonTutor/Java

## Problem 1: Long tracking time

This problem occurs when the Java program becomes complicated and has more execution steps.

**What PythonTutor/Java does**

Why it's slow? Here is what it does in a typical scenario:

1. The user visits visualize.html and types in Java code in the web-based text editor.

2. The user hits the "Visualize execution" button.

3. The frontend sends the user's Java code as a string to the backend by making an Ajax GET request.

4. The backend executes the Java code under the supervision of the java_jail,
    produces an execution trace, and sends that trace back to the frontend in JSON format.

5. The frontend switches to a visualization display, parses the execution trace,
    and renders the appropriate stack frames, heap objects, and pointers.

6. When the user interacts with the frontend by stepping through execution points,
    the frontend renders the proper data structures without making another subsequent call to the backend.

The stateless server does not maintain any information for an execution.
It generates the trace result all in one time.
If the program takes a while to execute, the user may suffer from a long period before the visualization.

**Solution of `PythonTutor/Java`**

To prevent long execution time and large JSON String from happening,

The server must limit the number of steps of the trace to prevent large JSON string and long execution time.
In other words, the tracer stops after a number of tracking steps.
(Well, the number of maximum steps can be adjusted to an acceptable size.)

For example, the `Java Visualizer` limits it at 510 steps, while `PythonTutor/Java` 254 steps.

**Analyze & Our Solution**

Q: is the limit of  510 steps acceptable?
In most of the time, yes. However, we can do better than that
and offer a more comfortable user experience.

*solution 1: keep the JVM alive*

This is the first solution that we have imagined.
The architecture looks like the client is controlling and debugging a JVM on the server via HTTP requests.

The server launches a JVM for the client and starts tracing the execution.
After a number of traces, it sends the JSON result to the client so that the client can enter into the visualization.
Then the server suspends the JVM.

After certain steps of visualization, the client requests to the server for some other steps.
The server resumes the JVM and continues tracking and sending JSON data.

A very challenging issue to this solution is that the server cannot support simultaneously too much client
because JVMs consume RAM and we have a physical memory limit.

For example, if the JVM runs at average 64M RAM, a server with 2GB can only maintain at maximum 32 clients.
This might not be acceptable.


*solution 2: break JSON into blocks*
Let's weaken this limit!

The problem comes from the architecture of `PythonTutor`.
It generates all the data at a time and send all of them in one JSON String.

What if, we break it down into multiples, and then send them block by block?
Once the client receives the first block, it switches to the visualization page as usual.
During the visualization, the server sends **silently** the other blocks to the client.

For example, we have a program that have totally 500 steps.
Let's compute the time to wait before entering into the visualization for two cases.

*case 1: do not break JSON in blocks*


* Sending an HTTP request takes up to hundreds of milliseconds
* Compiling the code takes 1 sec
* Tracing 500 steps takes 3 secs
* Downloading the JSON string of 10KB takes 200 milliseconds

Hence, the user waits up to 5 secs before entering into the visualization.

When the server is less performant or is overloaded, the tracing time might takes up to 10 seconds.
In this case, the client waits 12 secs, which would be an unacceptable result.

*case 2: break JSON into blocks*

To conquer this, the server can generate the first 100 tracks (2KB) and send to the client.
Then the client switches to the visualization view first, and it receives the rest JSON during the visualization.
This process continues until all data received.

* Compiling the code takes 1 sec.
* Tracing 100 steps takes 600 milliseconds.
* Downloading the first block of JSON data takes about 40 milliseconds.

In this case, the client waits less than 2 seconds.
If the server takes up to 10 seconds to generate 500 steps, then tracing 100 steps takes about 2 seconds.
The client only need to waits for 3 seconds.

In the meanwhile, since the program finishes the execution and the client posseses all necessary data for visualization,
the server can turn the JVM down to free the RAM.

To conclud, this approche has the following interests:

* small JSON string in each HTTP request.
* short waiting time before entering visualization, which leads to a better user experience.
* no memory issues on the server side, can still support almost the same amount of connections simultaneously as `PythonTutor/Java`.

However, it also has some fall-backs:

* We create more HTTP requests/response, but this does no effect in our case.
* More complex for both server and client.
    The server needs to dump the JSON multiple times during the execution.
    The client needs to store all the JSON and determine which to use.
    Also, it has to manage the situation that the user reacts faster than the server.
    (Even if it might be rare when the block is large enough)

## Problem 2: Single Java file

In OOP, it's common to have one class per file.
`PythonTutor/Java`, however, has only one file, thus we can only write internal static classes
for demonstrating some OOP concepts (e.g inherence).
When instantiating this classes, we have to prefix them with an outer class,
which is not absolutely not obvious for a novice.

We have to support multiple files, and consider at least the following subjects:

* The client needs a more dynamic code editor for editing multiple files.

* What if there are multiple mains function or there is no main function?
    We can add an constraint: always check if there is one and only one main.

* Can server compile multiple files and then launch the VM?
    This will need a furthur exploration into the `jdi`(Java Debug Interface) package.

* During the visualization, how to switch current lines among different files?
    This might takes time to implement on the frontend side.


## Problem 3: Nonconfigurable during the visualization

The `PythonTutor/Java` reuses the same front-end code as `PythonTutor`.

There are some options of visualization before running the tracer.
Different options give us different JSON data. (The tracer tracks in different ways)

We want to control this views during the visualization instead of before generating the trace.
Therefore, the Java tracer should produce more generic JSON results, with more detailed information.


## Problem 4: Unable to skip a call

When encountered a function call, the `PythonTutor/Java` have to go inside the call, this can be annoying
if the call is not interesting for us. (e.g. initializations of objects)

Hence, besides `step-in`, we also need to `step-over` a call.
The frontend needs to know the call stops in which step so that it can jump to taht step and rander the result.


## Problem 5: No multi-threading

The `PythonTutor/Java` supposes there is only one thread.

For our OOP courses, we might also need to demonstrate the concept of multi-threading.
So we have to trace all threads with the `jdi` package and each thread will have a stack of frames.
In the GUI, we need to show different stacks of frames.

[next chapter](./3-features-of-java-tutor.md)
