# Get started for developer

## Backend

To build the backend, you will need `gradle` and `java 1.8.x`.
Check your java version by running `$> java -version`.

You can checkout [this doc](deploy.md) for how to install `java 8` and `gradle`.

* build:

        $> cd backend/
        $> gradle build


* A `jar` file is generated after build, let's run it:

        $> java -jar build/libs/<app_name>.jar


You need to re-build the backend everytime when the backend code is changed.

If you want to use an IDE for the developpement,
see [this doc](./backend_dev_setup.md).


## Frontend

* To run the frontend server locally, you should have `npm` installed on your machine.
Then you should install `bower` and `grunt` with `npm`:

        $> sudo npm install -g bower
        $> sudo npm install -g grunt

* install dependencies:

        $> cd frontend/
        $> npm install
        $> bower install

* run the frontend server:

        $> grunt serve

Then a page is opened on your browser.
Each time your front-end code is changed, the page is reloaded automatically.

We recommand using an IDE for the front-end developpement (we used WebStorm).


