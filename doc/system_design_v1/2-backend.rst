JavaTutor's Backend
===================

The backend is a ``spring boot`` application.

The framework is only used for HTTP request and response.

Once there is a request from a client, we extract the JSON and
start the tracer.

The Java tracer is extracted from ``java_jail``.
Its request and response initially followed the ``OnlinePythonTutor`` JSON format.
`see here <https://github.com/pgbovine/OnlinePythonTutor/blob/master/v3/docs/opt-trace-format.md>`_
We have slightly modified the tracer to support multiple files.

You might need to read the JSON format document to understand the following sections.


Multiple files support
----------------------

usercode
++++++++

The ``usercode`` sent from the client is a list of files, instead of a single file.

Since the list does not contain the filename, and the file name need to be the same as
the public class, we need to extract the name of the public class of each file.

**To be improved**

A limit of ``usercode`` design: we can not compile a file with public interface or enum,
because we always consider that a Java file contains one public class.

Therefore, the ``usercode`` from the JSON be an object, with file name as key,
and Java code as value.

To extract this information on the backend is trivial, however, we need much
more complicated file managing features on the frontend.


trace
+++++

During the visualization, in order to identify the code position of each step,
we not only need to know the code's line number, but also the file name of that code.
Therefore, we have added a ``source`` field on each trace, which works perfectly.


Amend the trace
---------------

Consider the following example::

  public class Main {
    public Object foo() {
      return new Object();
    }

    public static void main(String [] args) {
      Object [] objectTable = {foo(), foo(), foo()};
    }
  }

We call a function to create some instances in the heap.
After creating the first instance by calling ``foo``, we don't have a variable refers to it,
so this instance becomes unreachable in the second call of ``foo``.
Similarly, when in the third call of ``foo``, both of the first and second created objects are
unreachable.

However, when the ``objectTable`` is created, all the three instances become reachable.
Since our tracer records the reachable objects in the heap, the objects created by ``foo`` disappear
and appear again, which is not reasonable and hard to explain to the users.

Thus, after tracing a program, we try to amend the trace, that is, we amend the heap of the
interval steps with the missing objects.


**Drawback**

Amending the trace requires complete trace history.
As a consequence, implementation of sending the JSON results block by block during the execution
may cause this bug when in interval steps are separated into two different blocks.

If the block size is large enough, the possibility of running into this bug is in fact negligible.

Well, breaking down the trace block by block will require a serious JSON format design,
and a lot of courage to implement.
