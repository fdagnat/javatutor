JavaTutor's Front-end
=====================

The front-end is built with AngularJS. You might need some basic
knowledge about AngularJS to understand how the app works.

The project was kickstarted using yeoman (http://yeoman.io/) and it
keeps using the MVC pattern. Understanding yeoman would be a great help
to understand JavaTutor's front-end.


MVC code structure
------------------

In this section, we will explain the most important part of the ``app`` directory.
In the project, you will find out more files.

Here is how the code is structured::

  app
    ├── index.html
    ├── scripts
    │   ├── app.js
    │   ├── controllers
    │   │   ├── config.js
    │   │   ├── edit_code.js
    │   │   └── visualize.js
    │   └── jtservice.js
    ├── styles
    │   └── main.css
    └── views
        ├── code_area.html
        ├── config.html
        ├── edit_code.html
        ├── stack_heap.html
        └── visualize.html

* The user starts with ``index.html``, which includes all the scripts, dependencies and styling files.

* ``scripts/app.js`` is the entry point of the application.

* ``scripts/jtservice.js`` is a served to create connections between DOM objects.

* All the logic control are placed under ``scripts/controllers/`` directory.

* All the presentation layer are placed in ``views/`` directory.
  Normally, each controller corresponds at least one view.
  Some views are broken down into multiple subviews.
  For example, ``visualize.html`` contains a ``code_area.html`` and a ``stack_heap.html``.


Local Storage
-------------

Browser's local storage allow us to store some data permanently.
We use this technique to pass data from one page to another, and also to memorize
the data.

1. Code and trace data

   After sending the code to the server, the client waits for the visualization data.
   Once the server returns a success trace, the client save the code and the trace into
   local storage, and then jump to the visualization page.

   On the visualization page, it gets the code and the trace from local storage for
   the visualization.


2. Configuration

   By default, the server's URL is ``http://localhost:8080``. Since the server may run
   on a different address or a different port, we add a configuration page that allows
   us to customize the server's URL. This configuration information is stored in local
   storage.


Connections between objects
---------------------------

As you can see, the objects and reference variables are connected with each other with arrows.

We create these arrows by using ``jsPlumb`` and we have encapsulated arrows' creation
into ``jtservice.js``. ("jt" means java tutor :D)

Each time when the user moves from one step to another, we re-calculate and repaint
all the connections.

We need the id DOM objects to create the connections.
Note that one connection is made with a source and a target.

In order to identify different objects and frames, we have created some rules.

**Source**

There are 4 kinds of sources, here are their formats::

1. static fields::

  SOURCE-STATIC-<var_name>-<ref_id>

For example, class ``A`` have a static reference ``foo``, which refers to the object
with id equals to 411. Then the DOM id of this field is ``SOURCE-STATIC-A.foo-411``


2. stack reference::

  SOURCE-<frame_id>-<var_name>-<ref_id>

Stack references are usually local variables.

For example, when we call a method named ``bar()``, we have a local
variable ``Object o = new Object()``.  Assume that the frame id of
this method call is 41, and the id of created object is 555.
Then the DOM id of the variable ``o`` is ``SOURCE-41-o-555``.


3. instance's attribute::

 SOURCE-<instance_id>-<var_name>-<ref_id>


4. array items::

  SOURCE-<array_id>-<array_index>-<ref_id>


**Target**

The targets are the objects or arrays created on the heap. Their DOM id is of the same format::

    TARGET<obj_id>


For example, if an object's id is 411, the it's DOM id is ``TARGET411``.

With a source's DOM id, we can easily find out its target id.
