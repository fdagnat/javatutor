What next?
==========

Basically, the first version of JavaTutor works.
However, there is still a lot of things that we should improve.

1. Better coding experience.

   For the moment, on the code editing page, we can only create and delete
   files. The current implementation does not allow us to create a file with public
   interface. To do so,  we need also be able to create files with names,
   and be able to rename the files.


2. Security problem.

   Since we execute the code directly on the server, the user's code can access
   the whole file system. might need to use ``chroot`` to limit the code's execution
   environment.


3. Send trace block by block

   We didn't have enough time to develop this subject for the first version.
   However, this functionality will loosen a lot the app's limits.
   The implementation could be challenging.


4. More features for visualization

   We may want the user to know that a string object is made by a table of characters.
   We want the objects to be more interactive: we can hide/show the connections etc.


5. We need automatic tests.

   The current project is tested manually. Since the project is still small, we don't
   have too much problem for the moment. However, we might really need unit test and
   functional test (using selenium) to ensure that the app always works correctly.

6. We need some basic examples.

   Sometimes the users do not know what to do with the tool. It would be greate we have
   some basic code examples.

(and maybe a lot more...)


I hope you have fun using this tool. :)
