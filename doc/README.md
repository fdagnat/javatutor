# Documentation

* [System design](./system_design_v1) describes the most important aspects of the
  first version of JavaTutor.

* [Dev guide](./dev_guide) shows you how to configure your developpement environment,
  and how to build the app.

* The [short report](./short_report) is written before JavaTutor is developped.
  The developpement was agile and didn't go exactly as discribed in this documentation.
